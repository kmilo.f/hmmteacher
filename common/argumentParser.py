import argparse
import os


class ArgumentParser(object):
    def __init__(self, argv, program_name, description):
        self.exec_arguments = argv

        self.ap = argparse.ArgumentParser(description=program_name + description)
        self.ap.add_argument('-f', '--file', dest='filename', required=True, type=self.existent_file,
                             help='Input JSON file')
        self.ap.add_argument('-d', '--debug', dest='debug', action='store_true')
        self.ap.add_argument('-o', '--output', required=True, type=self.dir_path, help='Output directory.')

    @staticmethod
    def dir_path(path):
        if os.path.isdir(path):
            return path
        else:
            raise argparse.ArgumentTypeError("readable_dir:{path} is not a valid path.")

    @staticmethod
    def existent_file(file_to_open):
        """
        'Type' for argparse - checks that file exists but does not open.
        """
        if not os.path.exists(file_to_open):
            # Argparse uses the ArgumentTypeError to give a rejection message like:
            # error: argument input: x does not exist
            raise argparse.ArgumentTypeError("{0} does not exist.".format(file_to_open))
        return file_to_open

    def return_arguments(self):
        """

        :return: Processed arguments.
        """
        return self.ap.parse_args(self.exec_arguments)
