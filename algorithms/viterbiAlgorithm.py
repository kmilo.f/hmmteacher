from common.commonFunctions import CommonFunctions as Cf


class ViterbiAlgorithm(object):
    def __init__(self, hidden_states, emission_data, transition_data, prior_values_data, sequence):
        self.hidden_states = hidden_states
        self.emission_data = emission_data
        self.transition_data = transition_data
        self.prior_values_data = prior_values_data
        self.sequence = sequence

    def execute_algorithm(self):
        """
        Viterbi Algorithm

        Initialization:

        Recursion:

        Termination:

        :return:
        """

        # Text Variables
        initialization_text = '<br><b>T: </b>' + str(1) + '<br>'
        recursion_text = ''
        pos = ''
        termination_text = ''

        # Calculations variables
        viterbi_matrix = [{}]

        for i, state in enumerate(self.hidden_states):
            viterbi_matrix[0][state] = {
                "prob": self.prior_values_data[state] * self.emission_data[state][self.sequence[0]], "prev": None}
            # Start Text #
            initialization_text += '<br><b>i:</b> ' + str(i + 1) + '<br>&delta;<sub>' + str(
                i + 1) + '</sub>(S<sub>' + str(i + 1) + '</sub>) = &Pi;<sub>' + str(
                i + 1) + '</sub> * emi(1,' + str(i + 1) + ') <br>&delta;<sub>' + str(
                i + 1) + '</sub>(S<sub>' + str(i + 1) + '</sub>) = ' + str(self.prior_values_data[state]) + ' * ' + str(
                self.emission_data[state][self.sequence[0]]) + ' = ' + str(viterbi_matrix[0][state]["prob"])
            # End Text #
        # When t > 0
        for t in range(1, len(self.sequence)):
            # Start text #
            pos += '<br><br><b>T: </b>' + str(t + 1) + '<br>'
            # End Text #
            viterbi_matrix.append({})
            for i, state in enumerate(self.hidden_states):
                max_transition_prob = viterbi_matrix[t - 1][self.hidden_states[0]]["prob"] * self.transition_data[
                    (self.hidden_states[0], state)]
                previous_selected_state = self.hidden_states[0]
                for previous_state in self.hidden_states[1:]:
                    transition_prob = viterbi_matrix[t - 1][previous_state]["prob"] * \
                                      self.transition_data[(previous_state, state)]
                    if transition_prob > max_transition_prob:
                        max_transition_prob = transition_prob
                        previous_selected_state = previous_state
                    else:
                        pass

                max_probability = max_transition_prob * self.emission_data[state][self.sequence[t]]
                viterbi_matrix[t][state] = {"prob": max_probability, "prev": previous_selected_state}

                # Start Text #
                recursion_text += pos
                recursion_text += '<br><b>i:</b> ' + str(i + 1) + '<br>&delta;<sub>' + str(
                    t + 1) + '</sub>(S<sub>' + str(i + 1) + '</sub>) = emi(' + str(t + 1) + ',' + str(
                    i + 1) + ') * max([&delta;<sub>' + str(t + 1) + '-1' + '</sub> * (S<sub>' + str(
                    i + 1) + '</sub>) * ' + 'trans(' + str(i + 1) + ',' + str(t) + ')]) <br> &delta;<sub>' + str(
                    t + 1) + '</sub>(S<sub>' + str(i + 1) + '</sub>) = ' + str(
                    self.emission_data[state][self.sequence[t]]) + ' * ' + str(max_transition_prob) + ' = ' + str(
                    viterbi_matrix[t][state]["prob"])
                pos = ''
                # End Text #
        opt = []
        max_probability = max(value["prob"] for value in viterbi_matrix[-1].values())
        previous = None
        # Get most probable state and it's backtrack
        for st, data in viterbi_matrix[-1].items():
            if data["prob"] == max_probability:
                opt.append(st)
                previous = st
                break
        # Follow the backtrack until the first observation
        for t in range(len(viterbi_matrix) - 2, -1, -1):
            opt.insert(0, viterbi_matrix[t + 1][previous]["prev"])
            previous = viterbi_matrix[t + 1][previous]["prev"]

        termination_text += '<br><b class="subtitle">The most probable sequence of hidden states is: <br>' + ' '.join(
            opt) + ' <br> P(Q|O) = %s' % max_probability + '</b>'

        viterbi_matrix_pd, viterbi_matrix_html = Cf.generate_pd_matrix_html(received_matrix=viterbi_matrix,
                                                                            hidden_states=self.hidden_states,
                                                                            sequence=self.sequence,
                                                                            highlight=True,
                                                                            viterbi=True,
                                                                            backtracking=opt)
        # print(viterbi_matrix_html)
        return opt, initialization_text, recursion_text, termination_text, viterbi_matrix_pd, viterbi_matrix_html
