import json


class ParseJson(object):
    def __init__(self, file):
        self.file = file
        self.json_data = None

    def deserialize(self):
        with open(self.file, 'r') as json_file:
            self.json_data = json.load(json_file)

        return self.json_data
