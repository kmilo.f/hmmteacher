import pandas as pd
import numpy as np


class CommonFunctions(object):
    def __init__(self):
        """
        Common function used by the program itself.
        Refer to each function for details
        """
        pass

    @staticmethod
    def generate_pd_matrix_html(received_matrix, hidden_states, sequence, viterbi=False, highlight=False,
                                backtracking=None):
        """
        Generate a pandas Dataframe
        Shape of the received matrix has to be: 
            [{'F': 0.5, 'L': 0.0}, {'F': 0.2, 'L': 0.06667}, {'F': 0.08333350000000002, 'L': 0.0666720001}]
          
        :param viterbi:
        :param sequence:
        :param hidden_states:
        :param received_matrix:
        :param highlight:
        :param backtracking:
        :return: Pandas Dataframe and html string
        """

        def highlight_max(pd_data, color='lightgreen', counter=0):
            '''
            highlight the maximum in a Series or DataFrame
            '''
            # print(pd_data)
            attr = 'background-color: {}'.format(color)
            temp_dict = np.ones(pd_data.shape, dtype=bool)

            for i, pd_col in enumerate(pd_data.items()):
                for j, index_ in enumerate(pd_col[1].index):
                    if index_ == backtracking[i]:
                        temp_dict[j][i] = True
                    else:
                        temp_dict[j][i] = False

            return pd.DataFrame(np.where(temp_dict, attr, ''), index=pd_data.index, columns=pd_data.columns)

        if viterbi:
            data = np.array([item[state]["prob"] for state in received_matrix[0] for item in received_matrix])
        else:
            data = [received_matrix[x][i] for i in hidden_states for x in range(len(sequence))]
        data_matrix = np.reshape(data, (len(hidden_states), len(sequence)))
        matrix_pd = pd.DataFrame(data_matrix, index=[state for state in hidden_states],
                                 columns=[c + '<sub>' + str(i) + '</sub>' for i, c in enumerate(sequence, start=1)])

        if highlight:
            matrix_html = matrix_pd.style.apply(highlight_max, axis=None).set_table_attributes(
                'class="dataframe table table-sm"').to_html()
            # matrix_html = matrix_pd.style.apply(
            #     (lambda x: [('background: lightgreen' if z[0] == backtracking[i] else '')
            #                 for i, z in enumerate(x.iteritems())])
            # ).set_table_attributes(
            #     'class="dataframe table table-sm"').render()
        else:
            matrix_html = matrix_pd.style.set_table_attributes('class="dataframe table table-sm"').to_html()
        return matrix_pd, matrix_html
