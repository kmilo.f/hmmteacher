from common.commonFunctions import CommonFunctions as Cf


class ForwardAlgorithm(object):
    def __init__(self, hidden_states, number_hidden_states, prior_values_data, emission_data, transition_data,
                 sequence):
        self.hidden_states = hidden_states
        self.number_hidden_states = number_hidden_states
        self.sequence = sequence
        self.prior_values_data = prior_values_data
        self.emission_data = emission_data
        self.transition_data = transition_data

    def execute_algorithm(self):
        """
        Execute forward algorithm

        Initialization:

        Recursion:

        Termination:

        Accordingly to the provided information, the algorithm sums the last two calculated probabilities.
        The sum of the alphas of the last element of the sequence and the corresponding hidden state.

        Probably another way to calculate the probabilities, needs confirmation.

        forward_sum = sum(current_data[k] *
        self.transition_data[(k, self.hidden_states[-1])] for k in self.hidden_states)

        :return:
        """
        # Calculations variables
        forward_matrix = []
        previous_data = {}
        current_data = None

        # Text variables
        t = ''
        initialization_text = ''
        recursion_text = ''

        for i, observation in enumerate(self.sequence):
            current_data = {}
            # Start text #
            t += '<br><b>T: </b>' + str(i + 1) + '<br>'
            # End text #
            for j, state in enumerate(self.hidden_states):
                if i == 0:
                    forward_summation = self.prior_values_data[state]
                    # Start Text #
                    initialization_text += '<br><b>i:</b> ' + str(j + 1) + '<br>&alpha;(1,' + str(
                        j + 1) + ') = &Pi;<sub>' + str(j + 1) + '</sub> * emi(' + state + ',' + self.sequence[
                                               i] + ') <br>&alpha;(1,' + str(j + 1) + ') = ' + str(
                        self.prior_values_data[state]) + ' * ' + str(
                        self.emission_data[state][observation]) + ' = '
                    # End Text #
                else:
                    forward_summation = sum(
                        previous_data[k] * self.transition_data[(k, state)] for k in self.hidden_states)
                    # Start Text #
                    recursion_text += t
                    recursion_text += '<br><b>i:</b> ' + str(j + 1) + '<br>&alpha;(' + str(i + 1) + ',' + str(
                        j + 1) + ') = emi(' + str(j + 1) + ',' + str(i + 1) + ') * &Sigma;['
                    t = ''
                    # End Text #
                current_data[state] = self.emission_data[state][observation] * forward_summation

                # For text purposes #
                if i == 0:
                    initialization_text += str(current_data[state]) + '<br> '
                else:
                    recursion_sum_text = [
                        '[&alpha;((' + str(i + 1) + '-1),' + str(k + 1) + ') * trans(' + str(k + 1) + ',' + str(
                            j + 1) + ')] + ' for k, x in enumerate(self.hidden_states)]
                    recursion_sum_num = ['[' + str(previous_data[k]) + ' * ' + str(
                        self.transition_data[(k, state)]) + '] + ' for k in self.hidden_states]

                    for text_element in recursion_sum_text:
                        recursion_text += text_element
                    recursion_text = recursion_text[:-3] + ']<br> &alpha;(' + str(i + 1) + ',' + str(
                        j + 1) + ') = ' + str(self.emission_data[state][observation]) + ' * '

                    for num_element in recursion_sum_num:
                        recursion_text += num_element
                    recursion_text = recursion_text[:-2] + ' = ' + str(current_data[state]) + '<br> '
            t = ''
            # End for text purposes #
            forward_matrix.append(current_data)
            previous_data = current_data

        final_summation = sum([current_data[k] for k in self.hidden_states])

        # Start Text #
        termination_text = '&Rho;(O: ' + self.sequence + ') = '
        termination_sum_text = ['&alpha;(' + str(len(self.sequence)) + ',' + str(k + 1) + ')' for k, x in
                                enumerate(self.hidden_states)]
        for sum_item_text in termination_sum_text:
            termination_text += sum_item_text + ' + '
        termination_text = termination_text[:-2] + '<br> <b>&Rho;(O: ' + self.sequence + ') = '
        termination_sum_num = ['(' + str(current_data[k]) + ')' for k in self.hidden_states]
        for sum_item_num in termination_sum_num:
            termination_text += sum_item_num + ' + '
        termination_text = termination_text[:-2] + ' = ' + str(final_summation) + '</b>'
        # End Text #

        forward_matrix_pd, forward_matrix_html = Cf.generate_pd_matrix_html(received_matrix=forward_matrix,
                                                                            hidden_states=self.hidden_states,
                                                                            sequence=self.sequence)

        return forward_matrix, forward_matrix_pd, forward_matrix_html, final_summation, t + initialization_text, \
               recursion_text, termination_text
