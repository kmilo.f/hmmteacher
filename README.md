# HmmTeacher Python Core

## Introduction

HMMTeacher was created for the purpose of learning about Hidden Markov Models, its inner workings and details. It has
the 3 main algorithms (forward, backward and viterbi) integrated and programmed in such way that the calculation steps
are shown in its entirety. Lastly, an HTML report is generated with the detailed information.

* This program was developed in [Python] using mainly [Numpy] and [Pandas].
* This program is used to calculate HMMs in the web version of [HMMTeacher].

## Requirements

    pandas >= 0.25.1
    Jinja2 >= 2.10.1
    numpy >= 1.16.4
    bokeh >= 1.3.4

You can use a conda installation or pip to install this packages.

Example with pip

```bash
pip install -r requirements.txt
```

Example with conda

```bash
conda install numpy
```

## Usage

HMMTeacher expect a json file as input. This will be processed by the program and an HTML report will be created.

A common execution is like follows.

```bash
python mainHmmTeacher.py -f file.json -o output directory
```

This will process the 'file.json' and output the report to the desired directory.

### Json Template

A basic Json template for the program to work is shown below.

```
{
   "transitionMatrix":"0.45,0.55,0.2,0.8",
   "observedChain":"HHHTTTHHTHTHHTHHTHHT",
   "numberObservedStates":2,
   "observedStates":"HT",
   "numberHiddenStates":2,
   "emissionMatrix":"0.2,0.8,0.5,0.5",
   "priorValuesVector":"0.3,0.7",
   "algorithms":"fo,vi,ba",
   "backwardOptions":"F,6",
   "hiddenStates":"F,L"
}
```

Take in consideration that no validation is done to the data in the json file. So is up to the user to check the correct
format of it.


[Python]: <https://www.python.org/>

[Pandas]: <http://pandas.pydata.org/>

[bokeh]: <https://bokeh.pydata.org/en/latest/>

[Numpy]: <http://www.numpy.org>

[HMMTeacher]: https://hmmteacher.mobilomics.org/
