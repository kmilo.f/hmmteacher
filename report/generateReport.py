from jinja2 import Environment, PackageLoader
import datetime
import os
import uuid


class Report(object):
    def __init__(self, observed_chain, alphabet, alphabet_length, transition_data, emission_data, prior_values_data,
                 forward_init_text, forward_rec_text, forward_term_text, forward_matrix, backward_intro_text,
                 backward_rec_text, backward_term_text, forward_backward_matrix, viterbi_init_text, viterbi_rec_text,
                 viterbi_term_text, viterbi_matrix, hidden_states, number_hidden_states, backward_options, algorithms,
                 plot_bokeh_div, plot_bokeh_script, output_dir):
        self.forward_init_text = forward_init_text
        self.forward_rec_text = forward_rec_text
        self.forward_term_text = forward_term_text
        self.forward_matrix = forward_matrix
        self.backward_intro_text = backward_intro_text
        self.backward_rec_text = backward_rec_text
        self.backward_ter_text = backward_term_text
        self.forward_backward_matrix = forward_backward_matrix
        self.viterbi_init_text = viterbi_init_text
        self.viterbi_rec_text = viterbi_rec_text
        self.viterbi_term_text = viterbi_term_text
        self.viterbi_matrix = viterbi_matrix
        self.observed_chain = observed_chain
        self.alphabet = alphabet
        self.alphabet_length = alphabet_length
        self.transition_data = transition_data
        self.emission_data = emission_data
        self.prior_values_data = prior_values_data
        self.hidden_states = hidden_states
        self.number_hidden_states = number_hidden_states
        self.backward_options = backward_options
        self.output_dir = output_dir
        self.algorithms = algorithms
        self.plot_bokeh_div = plot_bokeh_div
        self.plot_bokeh_script = plot_bokeh_script
        file_id = uuid.uuid4().hex
        self.pdf_name = 'HMMTeacher_report_' + file_id + '.pdf'
        self.html_name = 'HMMTeacher_report_' + file_id + '.html'
        self.html_name_standalone = 'HMMTeacher_report_off_' + file_id + '.html'

    def create_report(self):
        # Environment variable to indicate where the files are located
        env = Environment(loader=PackageLoader('report', 'template'))
        # Initialize the templates
        template_web = env.get_template("report_web.html")
        template_web_standalone = env.get_template("report_web_standalone.html")
        # template_pdf = env.get_template("report_pdf.html")

        # to get the date/time
        now = datetime.datetime.now()

        # assigning global variables for the templates
        template_web_standalone.globals['now'] = datetime.datetime.utcnow
        template_web_standalone.globals['year'] = now.year

        # assigning global variables for the templates
        template_web.globals['now'] = datetime.datetime.utcnow
        template_web.globals['year'] = now.year

        # assigning global variables for the templates
        # template_pdf.globals['year'] = now.year
        # template_pdf.globals['now'] = datetime.datetime.utcnow

        # data variables for the template
        template_vars = {"title": "HMMTeacher Report",
                         "report_title": "HMM Teacher results",
                         "observed_chain": self.observed_chain,
                         "hidden_states": self.hidden_states,
                         "number_hidden_states": self.number_hidden_states,
                         "backward_options": self.backward_options,
                         "alphabet": self.alphabet,
                         "transition_data": self.transition_data,
                         "emission_data": self.emission_data,
                         "prior_values_data": self.prior_values_data,
                         "algorithms": self.algorithms,
                         "forward_init_text": self.forward_init_text,
                         "forward_rec_text": self.forward_rec_text,
                         "forward_term_text": self.forward_term_text,
                         "forward_matrix": self.forward_matrix,
                         "backward_intro_text": self.backward_intro_text,
                         "backward_rec_text": self.backward_rec_text,
                         "backward_ter_text": self.backward_ter_text,
                         "forward_backward_matrix": self.forward_backward_matrix,
                         "viterbi_init_text": self.viterbi_init_text,
                         "viterbi_rec_text": self.viterbi_rec_text,
                         "viterbi_term_text": self.viterbi_term_text,
                         "viterbi_matrix": self.viterbi_matrix,
                         "plot_bokeh_div": self.plot_bokeh_div,
                         "plot_bokeh_script": self.plot_bokeh_script}
        # Html output, has the Bootstrap CSS as default.
        # has only the main components to be displayed inside of a already made web page
        # <div>...</div> (No head tag, etc)
        ####
        # An option could be added to generate a full html or only the main components in further versions.
        #
        html_out = template_web.render(template_vars)

        # standalone output - full html with bootstrap css
        standalone_out = template_web_standalone.render(template_vars)

        # Change from bootstrap css to bulma css in the tables
        # if self.forward_matrix is not None:
        #     template_vars["forward_matrix"] = template_vars["forward_matrix"].replace(
        #         'class="dataframe table table-sm"',
        #         'class="dataframe table is-narrow"')
        # else:
        #     pass
        # if self.forward_backward_matrix is not None:
        #     template_vars["forward_backward_matrix"] = template_vars["forward_backward_matrix"].replace(
        #         'class="dataframe table table-sm"', 'class="dataframe table is-narrow"')
        # else:
        #     pass
        # if self.viterbi_matrix is not None:
        #     template_vars["viterbi_matrix"] = template_vars["viterbi_matrix"].replace(
        #         'class="dataframe table table-sm"',
        #         'class="dataframe table is-narrow"')
        # else:
        #     pass
        # PDF output with the Bulma CSS
        # pdf_out = template_pdf.render(template_vars)

        # html output - for online webpage
        with open(os.path.join(self.output_dir, self.html_name), "w") as fh:
            fh.write(html_out)

        # html output for offline mode
        with open(os.path.join(self.output_dir, self.html_name_standalone   ), "w") as fh:
            fh.write(standalone_out)

        # HTML(string=pdf_out, base_url=__file__).write_pdf(os.path.join(self.output_dir, self.pdf_name),
        #                                                   stylesheets=[
        #                                                       CSS(
        #                                                           string='@page { size: A4 landscape; margin: 0.5cm };'
        #                                                                  'td{word-wrap:break-word}')])

        # return os.path.join(self.output_dir, self.html_name), os.path.join(self.output_dir, self.pdf_name)
        return os.path.join(self.output_dir, self.html_name), os.path.join(self.output_dir, self.html_name_standalone)
