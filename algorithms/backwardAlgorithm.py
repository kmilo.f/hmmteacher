from common.commonFunctions import CommonFunctions as Cf


class BackwardAlgorithm(object):
    def __init__(self, hidden_states, backward_options, emission_data, transition_data, prior_values_data, sequence,
                 forward_summation, forward_matrix):
        self.hidden_states = hidden_states
        self.sequence = sequence
        self.backward_options = backward_options
        self.emission_data = emission_data
        self.transition_data = transition_data
        self.prior_values_data = prior_values_data
        self.forward_summation = forward_summation
        self.forward_matrix = forward_matrix

    def execute_algorithm(self):
        """
        Forward-Backward algorithm.

        This algorithm calculates the probabilities that a hidden state (j) produced the observed states in the
        position (i) of the observed sequence. It's necessary to have the forward probabilities calculated because
        this are used in the final step (termination).

        Initialization:
        Being L the last observed state.

        B(L, j) = 1 for all k where k goes from 1 to N

        Other versions of the algorithm use the transition data of the last step in this place.
        In our case, we're using 1 as the prob of being in the last step, accordingly to the algorithm
        but worth checking it out.
        backward_current_val[state] = self.transition_data[(self.hidden_states[i], state)]

        Recursion:

        Termination:
        As the algorithm respect, this is the termination of the forward-backward algorithm.
        This is an specific probability given by the user. All the other probabilities are calculated
        so we only retrieve the desired data.

        :return:
        """

        # Text variables
        intro_text = '<b class="title">Probability that the position ' + str(self.backward_options[1]) + ' (' + str(
            self.sequence[int(self.backward_options[1]) - 1]) + ') was emitted by hidden state ' + str(
            self.backward_options[0]) + '.</b><br> <b class="subtitle">Forward data: </b><br> &Rho;(O: ' + str(
            self.sequence) + ') = ' + str(self.forward_summation)
        recursion_text = ''
        t = ''
        termination = ''

        # Calculation variables
        backward_matrix = []
        backward_previous_value = {}
        backward_current_val = None

        def make_recursion(self, z, f):
            text = ['[trans(' + str(z + 1) + ',' + str(k + 1) + ') * emi(' + str(k + 1) + ',' + str(
                len(self.sequence) - f) + ') * &beta;(' + str(len(self.sequence) - f) + ',' + str(
                k + 1) + ')]' for k, x in enumerate(self.hidden_states)]
            return text

        # Calculation process
        # We added an '_' to the sequence, because we need an extra step to se the starting probabilities to 1.
        # Also we're traversing from the last character to the first -1
        for i, obs in enumerate(reversed(self.sequence[1:] + '_')):
            # Start text #
            t += '<br><br><b>T: </b>' + str(len(self.sequence) - i) + '<br>'
            # End text #
            backward_current_val = {}
            for j, state in enumerate(self.hidden_states):
                if i == 0:
                    backward_current_val[state] = 1

                    # Start text #
                    recursion_text += t
                    recursion_text += '<br><b>i:</b> ' + str(j + 1) + '<br>&beta;(' + str(
                        len(self.sequence) - i) + '-1,' + str(j + 1) + ') = &Sigma;'

                    init_sum_text = make_recursion(self, i, j)

                    for text_element in init_sum_text:
                        recursion_text += text_element
                    # TODO
                    # FIX, replace is in bad position
                    recursion_text += '<br>&beta;(' + str((len(self.sequence) - i) - 1) + ',' + str(j + 1) + ') = '
                    # here the self.sequence[-1] makes reference to the last element of the observed chain, given that
                    # in the beginning (i=0) is the one consulted.
                    init_sum_num = [('[' + str(self.transition_data[(state, k)]) + ' * ' + str(
                        self.emission_data[k][self.sequence[-1]]) + ' * ' + str(backward_current_val[state])) + '] + '
                                    for k in self.hidden_states]
                    for sum_element in init_sum_num:
                        recursion_text = recursion_text[:-3] + sum_element
                    recursion_text += ' @ '
                    t = ''
                    # end text #
                else:
                    backward_current_val[state] = sum(
                        self.transition_data[(state, l)] * self.emission_data[l][obs] * backward_previous_value[l] for l
                        in self.hidden_states)

                    # Start Text #
                    # Not so ugly fix, because we need all the data for the backward matrix (including the initials
                    # 1), but we do not need the last step (t = 1) in the text information.
                    if len(self.sequence) - i != 1:
                        recursion_text += t
                        recursion_text += '<br><b>i:</b> ' + str(j + 1) + '<br>&beta;(' + str(
                            len(self.sequence) - i) + '-1,' + str(j + 1) + ') = &Sigma;['

                        recursion_sum_text = make_recursion(self, i, j)

                        for text_element in recursion_sum_text:
                            recursion_text += text_element

                        recursion_text += '<br>&beta;(' + str((len(self.sequence) - i) - 1) + ',' + str(j + 1) + ') = '
                        recursion_sum_num = [('[' + str(self.transition_data[(state, k)]) + ' * ' + str(
                            self.emission_data[k][obs]) + ' * ' + str(backward_current_val[state])) + '] + ' for
                                             k in self.hidden_states]

                        for sum_element in recursion_sum_num:
                            recursion_text = recursion_text[:-3] + sum_element
                        recursion_text += ' @ '
                        t = ''
                    else:
                        pass
                    # end text #

            backward_matrix.insert(0, backward_current_val)
            backward_previous_value = backward_current_val
            # For text purposes #
            if i == 0:
                recursion_text = '<br><p class="h4">Recursion</p><br>' \
                                 '$$ \\beta(t-1, i)=\sum_{j=1}^{N} a_{i j} ' \
                                 '\cdot e_{q_{t}=S_{j}}\left(O_{t}\\right) \cdot \\beta(t, j) ' \
                                 '$$' + recursion_text
            else:
                result_data = [backward_matrix[0][k] for k in self.hidden_states]
                for data in result_data:
                    recursion_text = recursion_text.replace(' @ ', ' = ' + str(data), 1)
            t = ''
            # End for text purposes #

        # Final probability
        p_backward_matrix = sum(
            self.prior_values_data[l] * self.emission_data[l][self.sequence[0]] * backward_current_val[l] for l in
            self.hidden_states)

        if self.forward_summation == 0:
            from algorithms.forwardAlgorithm import ForwardAlgorithm as Fa
            forward_algorithm = Fa(hidden_states=self.hidden_states,
                                   number_hidden_states=len(self.hidden_states),
                                   prior_values_data=self.prior_values_data,
                                   emission_data=self.emission_data,
                                   transition_data=self.transition_data,
                                   sequence=self.sequence)
            forward_matrix, forward_matrix_pd, forward_matrix_html, forward_summation, forward_init_text, \
            forward_rec_text, forward_term_text = forward_algorithm.execute_algorithm()

            self.forward_summation = forward_summation
            self.forward_matrix = forward_matrix
        else:
            pass

        for_back_termination = self.forward_matrix[int(self.backward_options[1]) - 1][self.backward_options[0]] * \
                               backward_matrix[int(self.backward_options[1]) - 1][
                                   self.backward_options[0]] / self.forward_summation

        termination += '&Rho;(q<sub>' + self.backward_options[1] + '</sub> = ' + self.backward_options[0] + '|' + str(
            self.sequence) + ') = &alpha;(' + str(self.backward_options[1]) + ',' + str(
            self.hidden_states.index(self.backward_options[0]) + 1) + ') * &beta;(' + str(
            self.backward_options[1]) + ',' + str(
            self.hidden_states.index(self.backward_options[0]) + 1) + ') / &Rho;(O)'
        termination += '<br><b>&Rho;(q<sub>' + self.backward_options[1] + '</sub> = ' + self.backward_options[
            0] + '|' + str(self.sequence) + ') = (' + str(
            self.forward_matrix[int(self.backward_options[1]) - 1][self.backward_options[0]]) + ' * ' + str(
            backward_matrix[int(self.backward_options[1]) - 1][self.backward_options[0]]) + ')/' + str(
            self.forward_summation) + ' = ' + str(for_back_termination) + '<br></b>'

        forw_backw_matrix_pd, forw_backw_matrix_html = Cf.generate_pd_matrix_html(received_matrix=backward_matrix,
                                                                                  hidden_states=self.hidden_states,
                                                                                  sequence=self.sequence)

        return backward_matrix, forw_backw_matrix_pd, forw_backw_matrix_html, p_backward_matrix, intro_text, \
               recursion_text, termination
