from bokeh.models import HoverTool
from bokeh.plotting import figure
from bokeh.palettes import Category20
from bokeh.embed import components


class Plotting(object):
    def __init__(self, forward_matrix, forward_backward_matrix, forward_summation, viterbi_data, sequence, output_dir):
        self.forward_matrix = forward_matrix
        self.forward_backward_matrix = forward_backward_matrix
        self.forward_summation = forward_summation,
        self.viterbi_data = viterbi_data
        self.sequence = sequence
        self.output_dir = output_dir

    def post_dec(self, x, y):
        return (x * y) / self.forward_summation

    def create_plot_bokeh(self):
        post_dec_pd = self.forward_matrix.combine(self.forward_backward_matrix, self.post_dec)
        new_col_names = [char + '_' + str(i) for i, char in enumerate(self.sequence, start=1)]
        post_dec_pd.columns = new_col_names

        transpose = post_dec_pd.T
        columns = transpose.columns.to_numpy()
        index = transpose.index.to_numpy()

        p = figure(title='Posterior Decoding', x_range=index, x_axis_label='States', y_axis_label='P(Hidden State|O)',
                   toolbar_location="above", width=1200)
        for column, color in zip(columns, Category20[20]):
            line = p.line(x=index, y=transpose[column], legend_label=column, color=color, line_width=2)
            p.add_tools(HoverTool(renderers=[line], tooltips=[(column, "$y")], toggleable=False))
        p.legend.location = "center_right"
        p.xaxis.major_label_orientation = "vertical"
        script, div = components(p)

        return script, div
