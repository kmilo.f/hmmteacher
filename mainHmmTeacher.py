import sys
import itertools
import numpy as np
from common.argumentParser import ArgumentParser as Ap
from common.parseJson import ParseJson as Jp
import pandas as pd

from algorithms.forwardAlgorithm import ForwardAlgorithm as Fa
from algorithms.backwardAlgorithm import BackwardAlgorithm as Bw
from algorithms.viterbiAlgorithm import ViterbiAlgorithm as Vi

from report.generateReport import Report as Rep
from plots.createPLots import Plotting as Pl

program_name = 'HmmTeacherCore'
program_ver = '0.1.5'
description = 'HMMTeacher guides you in the learning of the mechanics of solving the main three HMM algorithms. ' \
              'From the input of the data of a pre-stated HMM, passing by choosing the questions asked and ' \
              'the algorithms to apply, to the final report, containing the step-by-step algorithms solution. ' \
              'Developed by Camilo Fuentes Beals.'


def main_module(args):
    arguments = Ap(argv=args, program_name=program_name, description=description)

    # This part of the code could be extended to include more debug messages
    if arguments.return_arguments().debug:
        intro = '\n############################################\n' \
                '# {p} {v}                     #\n' \
                '# Tool focused on teaching HMM             #\n' \
                '# for education.                           #\n' \
                '############################################'.format(v=program_ver, p=program_name)

        print(intro)
        print(arguments.return_arguments())
        print('Input File: ', arguments.return_arguments().filename)
        print('Output: ', arguments.return_arguments().output)
        print('\n')
    else:
        pass

    # This part of the code parse a json file.
    parser = Jp(file=arguments.return_arguments().filename)
    json_data = parser.deserialize()

    # Observed states
    observed_chain = json_data['observedChain']
    # Alphabet (observed states)
    alphabet = json_data['observedStates']
    alphabet_length = json_data['numberObservedStates']

    # Hidden States
    hidden_states = json_data['hiddenStates'].split(',')
    number_hidden_states = json_data['numberHiddenStates']

    # Matrices , Shaped with numpy and pandas
    emission_matrix_data = np.array(json_data['emissionMatrix'].strip().split(','), dtype='float')
    emission_matrix = np.reshape(emission_matrix_data, (number_hidden_states, alphabet_length))
    emission_pd = pd.DataFrame(emission_matrix, index=hidden_states, columns=[x for x in alphabet])
    transition_matrix_data = np.array(json_data['transitionMatrix'].split(','), dtype='float')
    transition_matrix = np.reshape(transition_matrix_data, (number_hidden_states, number_hidden_states))
    transition_pd = pd.DataFrame(transition_matrix, index=hidden_states, columns=hidden_states)
    # Prior Values
    prior_values_vector_data = np.array(json_data['priorValuesVector'].split(','), dtype='float')
    # Extra data
    algorithms = json_data['algorithms'].split(',')
    backward_options = json_data['backwardOptions'].split(',')

    # This part of the code transform the prior values data to a desired structure where the hidden states have a
    # starting value.
    # Ex
    # F: 1
    # L: 0
    prior_values_data = {}
    for i, pi in enumerate(hidden_states):
        prior_values_data[pi] = prior_values_vector_data[i]

    # This part of the code transform the transition data to a desired structure of all the possible hidden states
    # and his transitions probabilities in form of tuple dictionary.
    # Ex:
    # (F,F) : 0.8
    # (F,L) : 0.2
    transition_data = {}
    for i, p in enumerate(itertools.product(hidden_states, repeat=2)):
        transition_data[p] = transition_matrix_data[i]

    # This part of the code transform the emission data to a desired structure of al the possible emitted states
    # coming from a hidden state.
    # Ex.
    # F: {C: 0.5, S: 0.5}
    # L: {C: 0.9, S: 0.1}
    emission_data = {}
    for i, hs in enumerate(hidden_states):
        inner_dict = {}
        for j, letter in enumerate(alphabet):
            inner_dict[letter] = emission_matrix[i][j]
        emission_data[hs] = inner_dict

    # This part of the code transform a pandas dataframe in to an html table with style and render, passing the
    # desired html class as an attribute
    emission_pd_html = emission_pd.style.set_table_attributes('class="dataframe table table-sm"').to_html()
    transition_pd_html = transition_pd.style.set_table_attributes('class="dataframe table table-sm"').to_html()

    # Calculation variables
    forward_summation = 0
    forward_matrix = None
    forward_matrix_html = None
    forward_matrix_pd = None
    forw_backw_matrix_html = None
    forw_backw_matrix_pd = None
    viterbi_steps = None
    viterbi_matrix_html = None
    # Text variables
    forward_init_text = None
    forward_rec_text = None
    forward_term_text = None
    back_intro_text = None
    back_rec_text = None
    back_ter_text = None
    vi_init_text = None
    vi_termination_text = None
    vi_recursion_text = None
    # Plot variables
    plot_bokeh_div = ''
    plot_bokeh_script = ''

    if 'fo' in algorithms:
        forward_algorithm = Fa(hidden_states=hidden_states,
                               number_hidden_states=number_hidden_states,
                               prior_values_data=prior_values_data,
                               emission_data=emission_data,
                               transition_data=transition_data,
                               sequence=observed_chain)
        forward_matrix, forward_matrix_pd, forward_matrix_html, forward_summation, forward_init_text, forward_rec_text, \
        forward_term_text = forward_algorithm.execute_algorithm()

    if 'ba' in algorithms:
        backward_algorithm = Bw(hidden_states=hidden_states,
                                emission_data=emission_data,
                                transition_data=transition_data,
                                prior_values_data=prior_values_data,
                                sequence=observed_chain,
                                backward_options=backward_options,
                                forward_summation=forward_summation,
                                forward_matrix=forward_matrix)
        forw_back_matrix, forw_backw_matrix_pd, forw_backw_matrix_html, backward_prob, back_intro_text, back_rec_text, \
        back_ter_text = backward_algorithm.execute_algorithm()

    if 'vi' in algorithms:
        viterbi_algorithm = Vi(hidden_states=hidden_states,
                               emission_data=emission_data,
                               transition_data=transition_data,
                               prior_values_data=prior_values_data,
                               sequence=observed_chain)
        viterbi_steps, vi_init_text, vi_recursion_text, vi_termination_text, viterbi_matrix_pd, viterbi_matrix_html = \
            viterbi_algorithm.execute_algorithm()

    # For the moment the only algorithm that has a graph is forward_backward. so we are going to filter by that option.
    # We left the plot options emtpy because in the html template is also filtered by the selected algorithms.

    # forward_summation is the final value of forward algorithm.
    # forward_matrix (pd, html) has the values for the forward matrix
    # backward_prob has the final backward value
    # forw_back_matix (pd, html) has the values for the forward-backward matrix.

    if 'ba' in algorithms:
        plots = Pl(forward_matrix=forward_matrix_pd,
                   forward_backward_matrix=forw_backw_matrix_pd,
                   forward_summation=forward_summation,
                   viterbi_data=viterbi_steps,
                   sequence=observed_chain,
                   output_dir=arguments.return_arguments().output)

        plot_bokeh_script, plot_bokeh_div = plots.create_plot_bokeh()

    generate_pdf = Rep(observed_chain=observed_chain,
                       alphabet=alphabet,
                       alphabet_length=alphabet_length,
                       transition_data=transition_pd_html,
                       emission_data=emission_pd_html,
                       prior_values_data=prior_values_data,
                       forward_init_text=forward_init_text,
                       forward_rec_text=forward_rec_text,
                       forward_term_text=forward_term_text,
                       forward_matrix=forward_matrix_html,
                       backward_intro_text=back_intro_text,
                       backward_rec_text=back_rec_text,
                       backward_term_text=back_ter_text,
                       forward_backward_matrix=forw_backw_matrix_html,
                       viterbi_init_text=vi_init_text,
                       viterbi_rec_text=vi_recursion_text,
                       viterbi_term_text=vi_termination_text,
                       viterbi_matrix=viterbi_matrix_html,
                       hidden_states=hidden_states,
                       number_hidden_states=number_hidden_states,
                       backward_options=backward_options,
                       algorithms=algorithms,
                       plot_bokeh_div=plot_bokeh_div,
                       plot_bokeh_script=plot_bokeh_script,
                       output_dir=arguments.return_arguments().output)

    html_file, pdf_file = generate_pdf.create_report()
    print(html_file, pdf_file)


if __name__ == '__main__':
    main_module(sys.argv[1:])
